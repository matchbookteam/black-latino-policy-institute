  <footer id="footer">
    
    	<div id="copyright"><p>&copy; 2014 Black &amp; Latino Policy. All rights reserved.</p></div>
        <div id="minority"><p></p></div>
        
        
		
        <a href="http://twitter.com/BLPI2006" target="_blank"><img src="images/icon-twitter.png" width="32" height="32"></a>
        
        <a href="https://www.facebook.com/blackandlatinopolicyinstitute" target="_blank"><img src="images/icon-facebook.png" width="32" height="32"></a>
       
    </footer>