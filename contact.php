﻿<?php
	//require 'simplepie/simplepie.inc';
	//$feed = new SimplePie('http://www.leppertinc.com/blog/feed/')
    
    if($_GET['success'] == 1){
        ?>
        <script>
        alert('Thank you for contacting Black & Latino Policy Institute.');
        </script>
        <?php
    }
?>
<!doctype html>
<!-- Thank you Paul Irish! -->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Black & Latino Policy Institute - Contact</title>
  <meta name="description" content="Clark Quinn Public Affairs">
  <meta name="author" content="">
  <meta name="keywords" content="" />

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>
 
  
  
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A">
    </script>
    
    <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1)
    </script>
    
    
 
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A"></script>
 
  <script type="text/javascript" src="js/gfeedfetcher.js"></script>
  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8341437-30', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>

  <div id="container">
  <?php
include 'header.php';

?>
    <div id="main" role="main">
    	<div id="col-left">
        	<img src="images/contact-slider.jpg" width="786" height="168" alt="Contact" id="banner">
            
            <div id="content">
            
                <div id="news">
					<?php include('feed.php'); ?>
                    
                    

                </div>
                
                <div id="main-text">
                    <h1>Contact</h1>
                    
                    
               
<ul class="contact-list">
	<li><strong>Black & Latino Policy Institute</strong></li>
<li>140 N. First St.</li>
<li>Zionsville, IN 46077</li>
<li>Tel: 317-489-2740</li>
<li>Fax: 630-733-6307</li>
<li>Email: <a href="mailto: info@bl-pi.org">info@bl-pi.org</a></li>
</ul>

                   
                    
                
                    <p>Contact us using the form below</p>
                    
                    <form action="/contact_form.php" method="post" id="form-login">
                    	<label for="name">Your <span id="blue">Name*</span></label><br/>
                        <input name="name" type="name" required autofocus><br/>
                        	<script>
								$(document).ready(function() {
									if (!("autofocus" in document.createElement("input"))) {
										$("#name").focus();
									}
								});
                            </script>
                        
                        <label for="email">Your <span id="blue">Email*</span></label><br/>
                        <input name="email" type="email" required><br/>
                        
                        <label for="company">Your <span id="blue">Company*</span></label><br/>
                        <input name="company" type="company" required><br/>
                        
                     
                        <input type="hidden" name="redirect" value="index.php" />
                        
							
                        <label for="comments">Your <span id="blue">Questions/Comments</span></label><Br/>
                        <textarea id="comments" cols="50" rows="10" name="comments"></textarea><br/>
                        <input type="submit" value="SUBMIT" id="submit">
                    </form>


                    
                </div>
            
            </div>
            
        </div>
        
        <div id="col-right">
       <?php include('rightcolumn.php'); ?>
            
        </div>
        <div style="clear:both;"></div>
    </div>
    
  <?php include('footer.php');  ?>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab local jQuery -->
  <script src='js/libs/jquery-1.5.1.min.js'></script>
 <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1);
     $(document).ready(function() {
  // Handler for .ready() called.
      //stick the footer at the bottom of the page if we're on an iPad/iPhone due to viewport/page bugs in mobile webkit
if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
{
  
	// $("footer").css({'background-color' : 'yellow', 'font-weight' : 'bolder'});
     
     jQuery("footer").addClass("static");
};
  
});
    



    </script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <!-- end scripts-->
  
  
  <script src="js/cycle/jquery.cycle.all.min.js"></script>

  <script src="js/setup.js"></script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->




</body>
</html>