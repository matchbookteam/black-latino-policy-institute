﻿<!doctype html>
<!-- Thank you Paul Irish! -->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 
   <title>Black &amp; Latino Policy Institute</title>
  <meta name="description" content="Black & Latino Policy Institute">
  <meta name="author" content="">
  <meta name="keywords" content="" />

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>
 
 
 <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A">
    </script>
    
    <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1)
    </script>
    
    
  
  
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A"></script>
  <!--<script type="text/javascript" src="js/twitter.js"></script> -->
  <script type="text/javascript" src="js/gfeedfetcher.js"></script>
  
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8341437-30', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>

  <div id="container">
<?php
include 'header.php';

?>
    
  
        
    <div id="main" role="main">
    	<div id="col-left">
        	<img src="images/about-slider.jpg" width="786" height="168" alt="BLPI About" id="banner">
            
            <div id="content">
            
                <div id="news">
				<?php include('feed.php'); ?>
                    
                </div>
                
                <div id="main-text">
                
     
                
                         <h1>About</h1>


<h2>Vision</h2>

<p>To be the premier Black and Latino Policy Institute and alike institutes internationally.</p>

<h2>Mission Statement</h2>
<p>The Black and Latino Policy Institute is a public policy research organization that identifies, investigates, and promotes solutions to issues affecting Black and Latino communities.  </p>

 <h1>History</h1>
<p>In 2006, The Black & Latino Policy Institute was established to bridge gaps found in public policy. The goal of the organization is to provide valuable, credible, and well-researched information so that legislatures, policy makers and others can make informed decisions. Jose Evans, former Executive Director of Indiana’s Commission on the Social Status of Black Males, decided that Indiana, the United States and the world desperately needed and organization that would not be limited by personal agendas, political campaigns, or other controlling entities. Individuals deserve to know the truth about our educational systems, access to courts, and all other social issues. Jenny Sarabia, former Executive Director of Indiana Commission on Hispanic/Latino Affairs, agreed to partner with Jose to identify and expose problems faced by the Latino community aiding to better understand diversity. John Loflin and Tysha Harding Sellers are co-founders of the organization. </p>     
                
 <p><img src="images/bluelogo.png" alt="logo" /></p>              
                   

                    
                    
                </div>
            
            </div>
            
        </div>
        
        <div id="col-right">
				<?php include('rightcolumn.php'); ?>
            
        </div>
        
        <div style="clear:both;"></div>
    </div>
    
  <?php include('footer.php');  ?>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab local jQuery -->
  <script src='js/libs/jquery-1.5.1.min.js'></script>

 <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1);
     $(document).ready(function() {
  // Handler for .ready() called.
      //stick the footer at the bottom of the page if we're on an iPad/iPhone due to viewport/page bugs in mobile webkit
if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
{
  
	// $("footer").css({'background-color' : 'yellow', 'font-weight' : 'bolder'});
     
     jQuery("footer").addClass("static");
};
  
});
    



    </script>

  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <!-- end scripts-->
  
  
  <script src="js/cycle/jquery.cycle.all.min.js"></script>

  <script src="js/setup.js"></script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->




</body>
</html>