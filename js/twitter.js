// JavaScript Document

// JavaScript Document

/*
 Outputs a Twitter feed via the Google AJAX Feed API
 (http://code.google.com/apis/ajaxfeeds/).

 By Emmet Connolly (http://blog.thoughtwax.com/?id=310).
 Last update: 22 Apr 2007.
 
*/

google.load("feeds", "1");

function initialize() {
 // var feedURL = "http://twitter.com/statuses/user_timeline/" +
   //             THWX_twitter_id + ".rss";
                
 var feedURL ="https://api.twitter.com/1/statuses/user_timeline.rss?screen_name="+THWX_twitter_id;
  var feed = new google.feeds.Feed(feedURL);
  var THWX_num_posts = 2;
  feed.load(function(result) {
    if (!result.error) {
      var username = result.feed.title.substring(10);
      var container = document.getElementById("THWX_twitter_status");
      for (var i = 0; i < THWX_num_posts; i++) {
        var entry = result.feed.entries[i];
        var div = document.createElement("div");
        var title = entry.title.substring(username.length + 2) + " ";
		var date = entry.publishedDate.substring(0, 16); //
        div.appendChild(document.createTextNode(title));
		
		var carriage = document.createElement("br");
        div.appendChild(carriage);
        container.appendChild(div);
		
		
		
        var link = document.createElement("a");
        link.setAttribute('href', entry.link);
        var linkText=document.createTextNode(date); //
        link.appendChild(linkText);
        div.appendChild(link);
        container.appendChild(div);
		
		var carriage = document.createElement("br");
        div.appendChild(carriage);
        container.appendChild(div);
		
		var carriage = document.createElement("br");
        div.appendChild(carriage);
        container.appendChild(div);
      }
    }
  });
}

google.setOnLoadCallback(initialize);