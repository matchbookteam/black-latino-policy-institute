<!doctype html>
<!-- Thank you Paul Irish! -->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

   <title>Black &amp; Latino Policy Institute</title>
  <meta name="description" content="Black & Latino Policy Institute">
  <meta name="author" content="">
  <meta name="keywords" content="" />

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>
 
 
 <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A">
    </script>
    
    <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1)
    </script>
    
    
  
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A"></script>

  <script type="text/javascript" src="js/gfeedfetcher.js"></script>
  
   
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8341437-30', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>

  <div id="container">
<?php
include 'header.php';

?>
    
        
    <div id="main" role="main">
    	<div id="col-left">
        	<img src="images/expungement-slider.jpg" width="786" height="168" alt="BLPI - Expungement" id="banner">
            
            <div id="content">
            
                <div id="news">
					<?php include('feed.php'); ?>
                    
                    
                </div>
                
                <div id="main-text">
                
                
      
                        <h1>Expungement</h1>
                        
<p><a href="form/expungement-form.html" target="_blank">CLICK HERE TO BEGIN</a></p>

<p><strong>Who is Eligible to get their Record Expunged in Indiana?</strong></p>
<p>There are several variables that determine who is eligible to get their record expunged in Indiana. Misdemeanors, class D felonies, arrest records, and more can all potentially be eliminated from a person&rsquo;s criminal history. The first prerequisite for expungement eligibility is the type of conviction or charge a person was sentenced with; meaning either a misdemeanor, felony dropped to a misdemeanor, or felony. The second factor is time. Depending on the charge, there are rules on how many years a person must wait to expunge their record. For details on the legalities surrounding criminal expungement in Indiana, call</p>

 <h2>Indiana Expungement Eligibility Requirements</h2>

<p>In order to remove arrest and conviction records from your criminal history, you must pass the Indiana expungement eligibility requirements to move forward in your case. Here are some of the prerequisites and guidelines surrounding criminal expungement in Indianapolis, IN:</p>

<p><strong>ARREST RECORDS </strong></p>
<p>To Be Eligible:</p>
<ul>
<li>Must Wait 1 Year Following Arrest</li>
<li>No Conviction Resulted</li>
<li>No Current, Further, or Pending Charges</li>

</ul>
<p><strong>MISDEMEANORS </strong></p>
<p>To Be Eligible:</p>
<ul>
<li>Must Wait 5 Years Following Arrest</li>
<li>No Present Driver&rsquo;s License Suspension</li>
<li>No Current, Further, or Pending Charges</li>
<li>Must Have Completed All Sentence Requirements and Probation Conditions (Including Fines and Expenses)</li>
<li>No Other Criminal Convictions in the Past 5 Years</li>
</ul>
<p><strong>CLASS D FELONIES reduced to MISDEMEANORS </strong></p>
<p>To Be Eligible:</p>
<ul>
<li>Must Wait 5 Years Following Arrest</li>
<li>No Present Driver&rsquo;s License Suspension</li>
<li>No Current, Further, or Pending Charges</li>
<li>Must Have Completed All Sentence Requirements and Probation Conditions (Including Fines and Expenses)</li>
<li>No Other Criminal Convictions in the Past 5 Years</li>
</ul>
<p><em>*Examples of Misdemeanors include: OWI/DUI Cases, Public Intoxication, Check Fraud, Shoplifting, Personal Possession, Battery (without injury), and more. Call Craven, Hoover, &amp; Blazek, P.C. for more information about Misdemeanor charges in Indiana.</em></p>
<p><strong>CLASS D FELONIES</strong></p>
<p>To Be Eligible:</p>
<ul>
<li>Must Wait 8 Years Following Arrest</li>
<li>No Present Driver&rsquo;s License Suspension</li>
<li>No Current, Further, or Pending Charges</li>
<li>Must Have Completed All Sentence Requirements and Probation Conditions (Including Fines and Expenses)</li>
<li>No Other Criminal Convictions in the Past 8 Years</li>
<li>Not All Class D Felonies Can Be Expunged (i.e. Sex Crimes, Homicide, Human Trafficking, Perjury, etc.)</li>
</ul>
<p><em>*Examples of Class D Felonies include: OWI/DUI with Prior, Theft, Possession, Intimidation, Fraud, and more. Call Craven, Hoover, &amp; Blazek, P.C. for more information about Class D Felony expungement in Indiana.</em></p>
<p><strong>ADDITIONAL FELONIES</strong></p>
<p>To Be Eligible:</p>
<ul>
<li>Must Wait 8 Years Following Arrest</li>
<li>No Present Driver&rsquo;s License Suspension</li>
<li>No Current, Further, or Pending Charges</li>
<li>Must Have Completed All Sentence Requirements and Probation Conditions (Including Fines and Expenses)</li>
<li>No Other Criminal Convictions in the Past 8 Years</li>
<li>Court <strong>MAY</strong> or <strong>MAY NOT</strong> Expunge Records for these Felony Cases</li>
</ul>
<p><em>*Eligibility for Felony Expungement <strong>EXCLUDES</strong> convictions involving Sex Offenses, Elected Official Misconduct, or Felonies Resulting in Serious Bodily Injury. Call Craven, Hoover, &amp; Blazek, P.C. for more information about Felony expungement in Indiana.</em></p>
<p><strong>Cost of Criminal Expungement in Indiana</strong></p>
<p>The cost of criminal expungement in Indiana differs. It all depends on the severity of the conviction, the number of convictions, the location of the convictions, the age of the convictions, and court costs. At Craven, Hoover, &amp; Blazek, P.C., your first consultation is FREE. This means you have nothing to lose and only knowledge to gain, regarding your eligibility for criminal expungement. Contact a friendly and knowledgeable legal representative today at 317-489-2740 to schedule a free first consultation with our skilled attorneys. The cost of criminal expungement in Indiana doesn&rsquo;t have to be scary, so long as you trust our legal teams to get the ruling you deserve.</p>



                </div>
            
            </div>
            
        </div>
        
        <div id="col-right">
    		<?php include('rightcolumn.php'); ?>
        </div>
        
        <div style="clear:both;"></div>
    </div>
    
  <?php include('footer.php');  ?>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab local jQuery -->
  <script src='js/libs/jquery-1.5.1.min.js'></script>
 <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1);
     $(document).ready(function() {
  // Handler for .ready() called.
      //stick the footer at the bottom of the page if we're on an iPad/iPhone due to viewport/page bugs in mobile webkit
if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
{
  
	// $("footer").css({'background-color' : 'yellow', 'font-weight' : 'bolder'});
     
     jQuery("footer").addClass("static");
};
  
});
    



    </script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <!-- end scripts-->
  
  
  <script src="js/cycle/jquery.cycle.all.min.js"></script>

  <script src="js/setup.js"></script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->




</body>
</html>