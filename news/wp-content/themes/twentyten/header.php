<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="" />
  
  
<link rel="stylesheet" href="../../../../css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="/media/wp-content/themes/twentyten/blogfix.css" />

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>



<!-- Grab local jQuery -->
  <script src='../../../../js/libs/jquery-1.5.1.min.js'></script>
<script>
     $(document).ready(function() {
      
  // Handler for .ready() called.
      // stick the footer at the bottom of the page if we\'re on an iPad/iPhone due to viewport/page bugs in mobile webkit
if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
{
  
	// $("footer").css({'background-color' : 'yellow', 'font-weight' : 'bolder'});
     
     jQuery("footer").addClass("static");
};
  
});
</script>

  <!-- scripts concatenated and minified via ant build script -->
  <script src="../../../../js/plugins.js"></script>
  <script src="../../../../js/script.js"></script>
  <!-- end scripts-->
  
  
<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="../../../../js/libs/modernizr-1.7.min.js"></script>
 
  
  <!-- Twitter widget actually uses Google AJAX API so as to not depend on Twitter's unreliable servers -->
  
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A"></script>
 
  <script type="text/javascript" src="../../../../js/gfeedfetcher.js"></script>
  
  
  <script src="../../../../js/cycle/jquery.cycle.all.min.js"></script>

  <script src="../../../../js/setup.js"></script>
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8341437-30', 'auto');
  ga('send', 'pageview');

</script>


    
</head>

<body <?php body_class(); ?>>
<div id="container" class="hfeed">
    <header>
		<img src="/images/logo.png" width="230"  alt="Black & Latino Policy Institute Logo">
        
        <div id="tagline">
        	<h1>Empowering Communities for Positive Change</h1>
        </div>
    </header>
    
<?php include('nav.php'); ?>
