<?php
/**
 * Template Name: Client Template
 *
 * A custom page template  sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

 get_header(); ?>

		<div id="main">
        
        	<div id="col-left">
            	<img src="/../images/banner-media.jpg" width="786" height="168" alt="Leppert Inc banner - home page" id="banner">
                
                <div id="content">
                    <div id="news-blog">
                        <?php get_sidebar(); ?>                 
                    </div>
                    
                    <div id="main-text-blog">
                    
                    
                    	<div style="width:450px; color: white;	display: block;	float: none;	outline-style: none;	outline-width: 0px;	padding-right: 10px;		vertical-align: baseline;   ">
                    	
							<?php
                            /* Run the loop to output the posts.
                             * If you want to overload this in a child theme then include a file
                             * called loop-index.php and that will be used instead.
                             */
                             get_template_part( 'loop', 'page' );
                            ?>
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
            
            
            <div id="col-right">
                <h2>Inside <span id="blue">Scoop</span></h2>
                
                <div id="twitter">
            	<div id="twitter-widget-title">Leppert Inc</div>
                <a href="http://twitter.com/#!/MichaelLeppert"><img src="../../images/icon-twitter.png" id="twitter-widget-icon"></a>
            	<div id="THWX_twitter_status"></div>
                <div id="twitter-widget-join"><a href="http://twitter.com/#!/MichaelLeppert"><img src="../../images/twitter.png"></a></div>
            </div>
                
                <p>Follow us on Twitter and Facebook to get the latest on Indiana government and politics.</p>
    
    <p>Take a look inside Leppert Inc, we are much more than a lobbying firm.</p>
                
    

        	</div>
            
        <div style="clear:both;"></div>
        

			
		
		</div><!-- #main -->


<?php get_footer(); ?>
