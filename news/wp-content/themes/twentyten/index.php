<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="main">
        
        	<div id="col-left">
            	<img src="/../images/news-slider.jpg" width="786" height="168" alt="News" id="banner">
                
                <div id="content">
                    <div id="news-blog">
                        <?php get_sidebar(); ?>                 
                    </div>
                    
                    <div id="main-text-blog">
                    
                    
                    	<div style=";width:450px; color: white;	display: block;	float: none;	outline-style: none;	outline-width: 0px;	padding-right: 10px;	padding-top: 0px;	vertical-align: baseline;     ">
                    	
							<?php
                            /* Run the loop to output the posts.
                             * If you want to overload this in a child theme then include a file
                             * called loop-index.php and that will be used instead.
                             */
                             get_template_part( 'loop', 'index' );
                            ?>
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
            
            
            <div id="col-right">
             	<?php include('rightcolumn.php'); ?>
        	</div>
            
        <div style="clear:both;"></div>
        

			
		
		</div><!-- #main -->


<?php get_footer(); ?>
