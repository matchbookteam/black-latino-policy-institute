<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="main">

			<div id="col-left">
            	<img src="../../images/banner-home.jpg" width="786" height="168" alt="EvansLeppert banner - home page" id="banner">
                
                <div id="content">
                    <div id="news-blog">
                        <?php get_sidebar(); ?>                 
                    </div>
                    
                    <div id="main-text-blog">
                    
                    	<div style="height:550px;width:450px; color: white;	display: block;	float: none;	outline-style: none;	outline-width: 0px;	padding-right: 10px;	padding-top: 0px;	vertical-align: baseline;     overflow:scroll;overflow-x: hidden;">

							<?php if ( have_posts() ) : ?>
                            <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                            <?php
                            /* Run the loop for the search to output the results.
                             * If you want to overload this in a child theme then include a file
                             * called loop-search.php and that will be used instead.
                             */
                             get_template_part( 'loop', 'search' );
                            ?>
            <?php else : ?>
                            <div id="post-0" class="post no-results not-found">
                                <h2 class="entry-title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
                                <div class="entry-content">
                                    <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
                                    <?php get_search_form(); ?>
                                </div><!-- .entry-content -->
                            </div><!-- #post-0 -->
                            <?php endif; ?>
                        
                        
                        </div>
                    </div>
                </div>
                
            </div>




        
        	<div id="col-right">
                <h2>Inside <span id="blue">Scoop</span></h2>
                
                <div id="twitter">
            	<div id="twitter-widget-title">EvansLeppert</div>
                <a href="http://twitter.com/#!/evansleppert"><img src="../../images/icon-twitter.png" id="twitter-widget-icon"></a>
            	<div id="THWX_twitter_status"></div>
                <div id="twitter-widget-join"><a href="http://twitter.com/#!/evansleppert"><img src="../../images/twitter.png"></div>
            </div>
                
                <p>Follow us on Twitter and Facebook to get the latest on Indiana government and politics.</p>
    
    <p>Take a look inside EvansLeppert, we are much more than a lobbying firm.</p>
                
                
                
                <ul id="cycle"></ul>

        	</div>

            
            
            
		<div style="clear:both;"></div>
            
            
            
		</div><!-- #main -->

<?php get_footer(); ?>
