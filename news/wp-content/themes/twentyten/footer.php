<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


	<footer>
    	<div id="copyright"><p>&#169; <?php echo date('Y'); ?>. Black & Latino Policy Institute. All rights reserved.</p></div>
        <div id="minority"><p></p></div>
        
       
        <a href="http://twitter.com/BLPI2006" target="_blank"><img src="/../images/icon-twitter.png" width="32" height="32"></a>
        
        <a href="https://www.facebook.com/blackandlatinopolicyinstitute" target="_blank"><img src="/../images/icon-facebook.png" width="32" height="32"></a>
        
    </footer>


</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
