<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="main">
        
        	<div id="col-left">
            	<img src="/../images/news-slider.jpg" width="786" height="168" alt="News" id="banner">
                
                <div id="content">
                    <div id="news-blog">
                        <?php get_sidebar(); ?>                 
                    </div>
                    
                    <div id="main-text-blog">
                    	<div style="height:550px;width:450px; color: white;	display: block;	float: none;	outline-style: none;	outline-width: 0px;	padding-right: 10px;	padding-top: 0px;	vertical-align: baseline;     overflow:scroll;overflow-x: hidden;">

						
                        	<h1 class="page-title"><?php
                                printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
                            ?></h1>
                            <?php
                                $category_description = category_description();
                                if ( ! empty( $category_description ) )
                                    echo '<div class="archive-meta">' . $category_description . '</div>';
                        
                            /* Run the loop for the category page to output the posts.
                             * If you want to overload this in a child theme then include a file
                             * called loop-category.php and that will be used instead.
                             */
                            get_template_part( 'loop', 'category' );
                            ?>
                       </div>

                    </div>
                </div>
                
            </div>
            
            
            
            
            
            
            <div id="col-right">
              	<?php include('rightcolumn.php'); ?>

        	</div>
            
        <div style="clear:both;"></div>
            
            
        
        
        
        
        
				


		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
