﻿<?php
	require 'simplepie/simplepie.inc';
	$feed = new SimplePie('http://www.evansleppert.com/blog/feed/')
?>
<!doctype html>
<!-- Thank you Paul Irish! -->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>EvansLeppert - Media</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>
 
 
 <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A">
    </script>
    
    <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1)
    </script>
    
    
  
  <!-- Twitter widget actually uses Google AJAX API so as to not depend on Twitter's unreliable servers -->
  <script type="text/javascript">
  var THWX_twitter_id = 292665715;
  </script>
  <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAUb8usXGxA15eROjax-nDuhSjCZ6NYLpjcCptaNh2N8kjsQUKFRTvlNm4gV915kphXIEDbVjTbub_8A"></script>
  <script type="text/javascript" src="js/twitter.js"></script>
  <script type="text/javascript" src="js/gfeedfetcher.js"></script>
  
  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8341437-30', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>

  <div id="container">
    <header>
		<img src="images/logo-small.png" width="205" height="143" alt="EvansLeppert logo">
        
        <img src="images/tagline.png" width="492" height="29" alt="EvansLeppert tagline - Issue Management and Strategic Relationship" id="tagline">
    </header>
    
  <?php include('nav.php'); ?>
        
    <div id="main" role="main">
    	<div id="col-left">
        	<img src="images/banner-home.jpg" width="786" height="168" alt="EvansLeppert banner - client login" id="banner">
            
            <div id="content">
            
                <div id="news">
					<h2>Latest <span id="blue">News Updates</span></h2>
                    
                    <div id="feeddiv"></div>
                        
                        <script type="text/javascript">
                        
                        var feedcontainer=document.getElementById("feeddiv")
                        var feedurl="http://evansleppert.com/media/?feed=rss2"
                        var feedlimit=3
                        var rssoutput="<ul>"
                        
                        function rssfeedsetup(){
                        var feedpointer=new google.feeds.Feed(feedurl) //Google Feed API method
                        feedpointer.setNumEntries(feedlimit) //Google Feed API method
                        feedpointer.load(displayfeed) //Google Feed API method
                        }
                        
                        function displayfeed(result){
                        if (!result.error){
                        var thefeeds=result.feed.entries
                        for (var i=0; i<thefeeds.length; i++)
                        rssoutput+="<li><a href='" + thefeeds[i].link + "'>" + thefeeds[i].title + "</a></li>"
                        rssoutput+="</ul>"
                        feedcontainer.innerHTML=rssoutput
                        }
                        else
                        alert("Error fetching feeds!")
                        }
                        
                        window.onload=function(){
                        rssfeedsetup()
                        }
                        
                        </script>
                    
                    
                </div>
                
                <div id="main-text">
                    <h1>EvansLeppert <span id="blue">Media</span></h1>
                    
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df.</p>
                    
                    
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    <p>Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf. Tes sdfoigo s ofisd foijh df. Fsdiu ef kisdfjo wf eofdjsf.</p>
                    
                </div>
            
            </div>
            
        </div>
        
        <div id="col-right">
        	<h2>Inside <span id="blue">Scoop</span></h2>
            
            <div id="twitter">
            	<div id="twitter-widget-title">EvansLeppert</div>
                <img src="images/icon-twitter.png" id="twitter-widget-icon">
            	<div id="THWX_twitter_status"></div>
                <div id="twitter-widget-join">Join the conversation</div>
            </div>
            
            <p>Follow us on <a href="http://twitter.com/#!/EvansLeppert">Twitter</a> and <a href="http://www.facebook.com/profile.php?id=100002370865440">Facebook</a> to get the latest on Indiana government and politics.</p>

<p>Take a look inside EvansLeppert, we are much more than a lobbying firm.</p>
            
            <h2>Visit <span id="blue">Our Gallery</span></h2>
            
            <ul id="cycle"></ul>
            
            
        </div>
        
        <div style="clear:both;"></div>
    </div>
    
    <footer>
    	<div id="copyright"><p>© 2011. EvansLeppert. All rights reserved.</p></div>
        
        
		<a href="http://twitter.com/#!/EvansLeppert"><img src="images/icon-twitter.png" width="32" height="32"></a>
        <a href="http://www.facebook.com/profile.php?id=100002370865440"><img src="images/icon-facebook.png" width="32" height="32"></a>
    </footer>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab local jQuery -->
  <script src='js/libs/jquery-1.5.1.min.js'></script>
 <script type="text/javascript">
    google.load("feeds", "1") //Load Google Ajax Feed API (version 1);
     $(document).ready(function() {
  // Handler for .ready() called.
      //stick the footer at the bottom of the page if we're on an iPad/iPhone due to viewport/page bugs in mobile webkit
if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod')
{
  
	// $("footer").css({'background-color' : 'yellow', 'font-weight' : 'bolder'});
     
     jQuery("footer").addClass("static");
};
  
});
    



    </script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <!-- end scripts-->
  
  
  <script src="js/cycle/jquery.cycle.all.min.js"></script>
  <script src="js/jflickrfeed.js"></script>
  <script src="js/setup.js"></script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->




</body>
</html>