    <header>
		<img src="images/logo.png" width="230"  alt="Black & Latino Policy Institute Logo">
        
        <div id="tagline">
        	<h1>Empowering Communities for Positive Change</h1>
        </div>
    </header>
        
    <div id="nav">
    <?php
    
    //if($_SERVER['REMOTE_ADDR'] != '68.58.133.72') { header('location: http://google.com'); exit;}
    
     if($_SERVER['REQUEST_URI'] ==  '/' || $_SERVER['REQUEST_URI'] ==  '/index.php'  ) $home = 1;
     if($_SERVER['REQUEST_URI'] ==  '/about.php'  ) $about = 1;
     if($_SERVER['REQUEST_URI'] ==  '/services.php'  ) $services = 1;
     if($_SERVER['REQUEST_URI'] ==  '/contact.php'  ) $contact = 1;
	 if($_SERVER['REQUEST_URI'] ==  '/expungement.php'  ) $expungement = 1;
	 if($_SERVER['REQUEST_URI'] ==  '/history.php'  ) $history = 1;
    
    ?>
        	<ul>
            	<li><a href="index.php" <?php if(isset($home)){?>id ="current" <?php } ?>>Home</a></li>
                <li><a href="about.php" <?php if(isset($about)){?>id ="current" <?php } ?>>About</a></li>
                <li><a href="services.php" <?php if(isset($services)){?>id ="current" <?php } ?>>Services</a></li>
                <li><a href="expungement.php" <?php if(isset($expungement)){?>id ="current" <?php } ?>>Expungement</a></li>
                <li><a href="/news/">News</a></li>
                <li><a href="support-us.php" <?php if(isset($history)){?>id ="current" <?php } ?>>Support Us</a></li>
                <li><a href="contact.php" <?php if(isset($contact)){?>id ="current" <?php } ?>>Contact</a></li>
            </ul>
        </div>